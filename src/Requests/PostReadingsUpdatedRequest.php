<?php
namespace Shifft\P4\Requests;

use DateTime;

class PostReadingsUpdatedRequest extends BaseRequest
{
	protected function getEndpoint(): string
	{
		if(!is_a($this->params[0], DateTime::class))
		{
			throw new \InvalidArgumentException('PostReading requires third parameter to be a DateTime');
		}
		if(!is_integer($this->params[1]))
		{
			throw new \InvalidArgumentException('PostReading requires second parameter to be an integer');
		}
		return 'readings/updated/'.$this->params[0]->format('YmdHi').'/'.$this->params[1];
	}
	
	protected function requestMethod(): string
	{
		return 'POST';
	}
	
	protected function addRequestParams($ch): void
	{
		if(!is_array($this->params[2]))
		{
			throw new \InvalidArgumentException('PostAddress requires second parameter to be an array');
		}
		curl_setopt($ch, CURLOPT_POST, count($this->params[2]));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->build_post_fields($this->params[2]));
	}
}
?>