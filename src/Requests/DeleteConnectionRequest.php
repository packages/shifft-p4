<?php
namespace Shifft\P4\Requests;

use Shifft\P4\Token;

class DeleteConnectionRequest extends BaseRequest
{
	
	protected function getEndpoint(): string
	{
		if(!is_int($this->params[0]))
		{
			throw new \InvalidArgumentException('GetConneciton requires first parameter to be an integer');
		}
		return 'connections/'.$this->params[0];
	}
	
	protected function addRequestParams($ch): void
	{
		
	}
	
	protected function requestMethod(): string
	{
		return 'DELETE';
	}
}
?>