<?php
namespace Shifft\P4\Requests;

use Shifft\P4\Token;

class GetAddressesRequest extends BaseRequest
{
	
	protected function getEndpoint(): string
	{
		return 'addresses';
	}
	
	protected function addRequestParams($ch): void
	{
		
	}
}
?>