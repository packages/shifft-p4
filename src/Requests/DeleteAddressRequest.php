<?php
namespace Shifft\P4\Requests;

use Shifft\P4\Token;

class DeleteAddressRequest extends BaseRequest
{
	
	protected function getEndpoint(): string
	{
		if(!is_int($this->params[0]))
		{
			throw new \InvalidArgumentException('GetAddress requires first parameter to be an integer');
		}
		return 'addresses/'.$this->params[0];
	}
	
	protected function addRequestParams($ch): void
	{
		
	}
	
	protected function requestMethod(): string
	{
		return 'DELETE';
	}
}
?>