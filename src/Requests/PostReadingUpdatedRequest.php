<?php
namespace Shifft\P4\Requests;

use DateTime;

class PostReadingUpdatedRequest extends BaseRequest
{
    const ALLOWED_PARENTS = ['address', 'connection'];
	protected function getEndpoint(): string
	{
		if(!is_string($this->params[0]))
		{
			throw new \InvalidArgumentException('PostReading requires first parameter to be a string');
		}
		if(!in_array($this->params[0], static::ALLOWED_PARENTS))
		{
			throw new \InvalidArgumentException('PostReading requires first parameter to be address or connection');
		}
		if(!is_integer($this->params[1]))
		{
			throw new \InvalidArgumentException('PostReading requires first parameter to be an integer');
		}
		if(!is_a($this->params[2], DateTime::class))
		{
			throw new \InvalidArgumentException('PostReading requires first parameter to be a DateTime');
		}
		return 'readings/'.$this->params[0].'/'.$this->params[1].'/updated/'.$this->params[2]->format('YmdHi');
	}
	
	protected function requestMethod(): string
	{
		return 'POST';
	}
	
	protected function addRequestParams($ch): void
	{
		if(!is_array($this->params[3]))
		{
			throw new \InvalidArgumentException('PostAddress requires third parameter to be an array');
		}
		curl_setopt($ch, CURLOPT_POST, count($this->params[3]));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->build_post_fields($this->params[3]));
	}
}
?>