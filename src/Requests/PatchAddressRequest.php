<?php
namespace Shifft\P4\Requests;

use Shifft\P4\Token;

class PatchAddressRequest extends BaseRequest
{
	
	protected function getEndpoint(): string
	{
		if(!is_int($this->params[0]))
		{
			throw new \InvalidArgumentException('PatchAddress requires first parameter to be an integer');
		}
		return 'addresses/'.$this->params[0];
	}
	
	protected function requestMethod(): string
	{
		return 'PATCH';
	}
	
	protected function addRequestParams($ch): void
	{
		if(!is_array($this->params[1]))
		{
			throw new \InvalidArgumentException('PatchAddress requires second parameter to be an array');
		}
		curl_setopt($ch, CURLOPT_POST, count($this->params[1]));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->build_post_fields($this->params[1]));
	}
}
?>