<?php
namespace Shifft\P4\Requests;

use Exception;
use Shifft\P4\Exceptions\P4Exception;
use Shifft\P4\Token;

abstract class BaseRequest
{
	protected $token;
	protected $baseUrl;
	protected $params;

	public function __construct(Token $token, string $baseUrl, array $params)
	{
		$this->token = $token;
		$this->baseUrl = $baseUrl;
		$this->params = $params;
	}
	
	protected abstract function getEndpoint(): string;
	protected abstract function addRequestParams($ch): void;
	
	public function run(): \StdClass
	{
		$headers = [];
		$ch = curl_init($this->baseUrl.'/'.$this->getEndpoint());
		curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge($headers, $this->token->getHeaders()));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->requestMethod());
		
		$this->addRequestParams($ch);
		
		$output = curl_exec($ch);
		$httpdCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		$result = $output == null ? new \StdClass() : json_decode($output);
		if($httpdCode >= 300 && (!isset($result->data) || $result->data == null))
		{
			if($result->error)
			{
				throw new P4Exception($result->error->code, $result->error->message, $result->error->data);
			}
			else
			{
				throw new Exception('Something went wrong');
			}
		}
		
		return $result;
		
	}

	public function build_post_fields($data, $existingKeys = '', &$returnArray = []): array
	{
		if(($data instanceof CURLFile) or !(is_array($data) or is_object($data)))
		{
			$returnArray[$existingKeys]=$data;
			return $returnArray;
		}
		else
		{
			foreach ($data as $key => $item) 
			{
				$this->build_post_fields($item, $existingKeys ? $existingKeys."[$key]" : $key, $returnArray);
			}
			return $returnArray;
		}
	}
	
	protected function requestMethod(): string
	{
		return 'GET';
	}
}
?>