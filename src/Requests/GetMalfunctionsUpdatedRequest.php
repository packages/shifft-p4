<?php
namespace Shifft\P4\Requests;

use DateTime;

class GetMalfunctionsUpdatedRequest extends BaseRequest
{
	protected function getEndpoint(): string
	{
		if(!is_a($this->params[0], DateTime::class))
		{
			throw new \InvalidArgumentException('PostReading requires first parameter to be a DateTime');
		}
		return 'malfunctions/'.$this->params[0]->format('YmdHi');
	}
	
	protected function requestMethod(): string
	{
		return 'GET';
    }
    
	protected function addRequestParams($ch): void {}
}
?>