<?php
namespace Shifft\P4\Requests;

use Shifft\P4\Token;

class TestRequest extends BaseRequest
{
	protected function getEndpoint(): string
	{
		return 'test';
	}
	
	protected function addRequestParams($ch): void
	{
		
	}
}
?>