<?php
namespace Shifft\P4\Requests;

use DateTime;

class GetMalfunctionsRequest extends BaseRequest
{
    const ALLOWED_PARENTS = ['address', 'connection'];
	protected function getEndpoint(): string
	{
		return 'malfunctions';
	}
	
	protected function requestMethod(): string
	{
		return 'GET';
    }
    
	protected function addRequestParams($ch): void {}
}
?>