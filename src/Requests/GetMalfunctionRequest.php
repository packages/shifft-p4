<?php
namespace Shifft\P4\Requests;

use DateTime;

class GetMalfunctionRequest extends BaseRequest
{
    const ALLOWED_PARENTS = ['address', 'connection'];
	protected function getEndpoint(): string
	{
		if(!is_int($this->params[0]))
		{
			throw new \InvalidArgumentException('GetMalfunction requires first parameter to be an integer');
		}
		return 'malfunctions/connection/'.$this->params[0];
	}
	
	protected function requestMethod(): string
	{
		return 'GET';
    }
    
	protected function addRequestParams($ch): void {}
}
?>