<?php
namespace Shifft\P4\Requests;

use Shifft\P4\Token;

class PostAddressesRequest extends BaseRequest
{
	protected function getEndpoint(): string
	{
		return 'addresses';
	}
	
	protected function requestMethod(): string
	{
		return 'POST';
	}
	
	protected function addRequestParams($ch): void
	{
		if(!is_array($this->params[0]))
		{
			throw new \InvalidArgumentException('PostAddress requires first parameter to be an array');
		}
		curl_setopt($ch, CURLOPT_POST, count($this->params[0]));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->build_post_fields($this->params[0]));
	}
}
?>