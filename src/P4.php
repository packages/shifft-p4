<?php
namespace Shifft\P4;

use Shifft\P4\Exceptions\{NonExistingRequestException, TokenException};

class P4
{
	private $url;
	private $username;
	private $password;
	private $scope;
	private $token;

	/**
	 *  @brief create a p4 model and logs in
	 *  
	 *  @param string $url the url to the p4 API environment you want to connect
	 *  @param string $username the username with which you login
	 *  @param string $password the password that belongs to the username
	 *  @param string $scope the scope for the session, if you need all don't supply a scope
	 *  @return \Shifft\P4\P4
	 *  
	 *  @throws \Shifft\P4\Exceptions\TokenException
	 */
	public function __construct(string $url, string $username, string $password, string $scope = null)
	{
		$this->url = $url;
		$this->username = $username;
		$this->password = $password;
		$this->scope = $scope;
		$this->login();
	}
	
	/**
	 *  @brief handle a p4 request
	 *  
	 *  @param [in] $name the name of the request
	 *  @param [in] $params the parameters of the request
	 *  @return \StdClass
	 */
	public function __call(string $name, array $params): \StdClass
	{
		$className = '\\Shifft\\P4\\Requests\\'.ucfirst($name).'Request';
		if(!class_exists($className))
		{
			throw new NonExistingRequestException('De request '.$name.' is niet bekend.');
		}
		if(!$this->tokenIsValid())
		{
			$this->login();
		}
		$request = new $className($this->token, $this->url, $params);
		return $request->run();
	}
	
	/**
	 *  @brief returns if the token is valid
	 *  
	 *  @return bool
	 */
	public function tokenIsValid(): bool
	{
		return $this->token != null && $this->token->isValid();
	}
	
	/**
	 *  @brief request an access token from the p4
	 *  
	 *  @return void
	 *  
	 *  @throws \Shifft\P4\Exceptions\TokenException
	 */
	private function login()
	{
		//make the login request
		$ch = curl_init($this->url.'/token');
		$postData =
		[
			'grant_type'=>'password',
			'username'=>$this->username,
			'password'=>$this->password,
		];
		if($this->scope != null)
			$postData['scope'] = $this->scope;
		
		curl_setopt($ch, CURLOPT_POST, count($postData));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$json = json_decode($response);
		//if the http code is not 200, something went wrong
		if($httpCode != 200)
		{
			throw new TokenException($json->error_description, $json->error);
		}
		$this->token = new Token($json->access_token, $json->token_type, $json->expires_in, $json->scope);
	}
}
?>