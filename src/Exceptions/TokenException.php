<?php
namespace Shifft\P4\Exceptions;

class TokenException extends \Exception
{
	private $description;
	private $error;
	
	public function __construct(string $description, string $error)
	{
		$this->description = $description;
		$this->error = $error;
		parent::__construct($error.': '.$description);
	}
	
	public function __get(string $name): string
	{
		if($name == 'description' || $name == 'error')
		{
			return $this->$name;
		}
	}
}
?>