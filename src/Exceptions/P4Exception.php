<?php
namespace Shifft\P4\Exceptions;

class P4Exception extends \Exception
{
	private $errorCode;
	private $number;
	private $data;
	
	public function __construct(string $code, string $message, array $data = null)
	{
		parent::__construct($message);
		$this->errorCode = $code;
		$this->data = $data;
		$parts = explode('.', $code);
		$this->number = array_pop($parts);
	}

	public function __get(string $name)
	{
		if($name == 'errorCode' || $name == 'message' || $name == 'data' || $name == 'number')
		{
			return $this->$name;
		}
	}
}
?>