<?php
namespace Shifft\P4;

class Token
{
	private $token;
	private $type;
	private $expireStamp;
	private $scopes;
	
	public function __construct(string $accessToken, string $tokenType, int $expiresIn, string $scopes)
	{
		$this->token = $accessToken;
		$this->type = $tokenType;
		$this->scopes = explode(' ', $scopes);
		$this->expireStamp = time() + $expiresIn - 10;
	}
	
	public function isValid(): bool
	{
		return $this->expireStamp > time();
	}
	
	public function getHeaders(): array
	{
		return ['Authorization: Bearer '.$this->token];
	}
}
?>